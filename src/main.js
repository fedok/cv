import Vue from 'vue';
import App from 'src/App.vue';
import router from 'src/plugins/router';
import vuetify from 'src/plugins/vuetify';
import i18n from 'src/plugins/i18n/i18n';

// Vue.config.productionTip = false;

new Vue({
  router,
  vuetify,
  i18n,
  render: (h) => h(App),
}).$mount('#app');
