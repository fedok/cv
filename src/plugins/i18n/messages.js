/* eslint-disable vue/max-len */

const messages = {
  en: {
    message: {
      global_home: 'Home',
      global_about: 'About',
      global_hard_skills: 'Hard skills',
      global_career: 'Career',
      global_projects: 'Projects',
      global_print: 'Print',
      global_language: 'Language',
      global_stack: 'Stack',

      home_page_info_1: 'Welcome, on this page you will find the CV of Oleksandr Fedorenko.',
      home_page_info_2:
        'This site is divided into sections, for the ease of reading. If you need the whole version at once, go to the',
      home_page_info_3: 'Print',
      home_page_info_4: 'section',

      about_page_info: 'Here is a little bit of my biography and hobbies, not very much, within reason.',
      about_page_photo: 'Photo',
      about_page_intro: 'Introduction',
      about_page_intro_txt:
        "Hi, I'm not really sure if this will be read, but I'll leave this story here anyway. Let's start. My name is Alexander Fedorenko, I was born in 1995 in Kiev, where I live to this day.",
      about_page_study: 'Studying',
      about_page_study_txt:
        "I studied 9 classes at school, then entered a technical college, and in 2014 I got into the National Transport University with a degree in 'Bridge and Tunnel Construction'. Here you can notice that the specialty is not related to IT. It's not a mistake, I'm really not an IT person by education.<br>I graduated with a bachelor's and master's degree in 4 years, got a red diploma, and finished with a government education.",
      about_page_job: 'First normal job',
      about_page_job_txt:
        'Even before I finished university, I was offered a job in my specialty, I did not refuse and worked in this area for a total of 2 years, having changed two jobs. The specialty itself is not bad and interesting. Designing metal structures and reinforced concrete structures, visiting various factories, you can see with your own eyes how your drawings become a real building.<br>There was only one drawback: not high salaries and slow career progression.',
      about_page_move: "Knight's move",
      about_page_move_txt:
        "I consulted with my IT friends and made a decision not to go to courses because it required money, which I didn't have, but to try to learn something by myself.<br>Of course, I didn't quit my job as an engineer, I started to learn programming. My experience in .Net helped me. I used it to write my diploma thesis. As a result, in 3 months I wrote a simple website with some engineering calculations. I made my resumé and started sending it to almost every job. We all know how hard it is to get into IT, but it was not as hard as I thought.",
      about_page_career: 'New career',
      about_page_career_txt:
        "I usually don't write bad things about employers, but at this point I understand that the first company I joined was way behind in IT standards in everything except knowledge, although at the time I could only suspect it, it didn't scare me much.<br>In general, it was cool that they accepted newcomers, if there was a desire to learn, it was welcomed. I gained experience and decided to conquer new heights.<br>I got a job with a new company, but I didn't work very long, realized that I did not particularly like bots, and I went to another product company.<br>That's where I finally experienced the benefits of a good office, finally, about a year passed. On the whole, I worked well in the new company, nice people, office, covid, remote work, classics. I worked for them for a year, there were more responsibilities, but the funding remained the same, so I went to another company.<br>Well, I still work for this new company, I managed to gain experience in different areas while running for work, and in my last job I discovered a new direction, the blockchain.",
      about_page_outside: 'Outside of work',
      about_page_outside_txt:
        "In between jobs, I've had two fairly extensive projects. The first, a schedule management site for various universities with admin, authorization, etc. The difficulty is that different universities have different approaches and this needs to be put together, overall it was pretty interesting. The second: decentralized game, that was a more fun project. Also attended events like hackathons, even managed to collect a couple of prizes, but not the main ones.",
      about_page_hobby: 'Hobby',
      about_page_hobby_txt:
        'I like different activities, especially extreme ones: shooting, skydiving, hiking, volleyball, swimming... More sedentary activities: reading, doing puzzles, making something with my hands.',

      hard_skill_page_info: 'This is my personal approximation of technical skills.',
      hard_skill_page_grad: 'Grading scale',
      hard_skill_page_grad_1: 'encountered, but without active development.',
      hard_skill_page_grad_2: 'worked and wrote a little in this language or framework.',
      hard_skill_page_grad_3: "used it in my work, I won't get lost if I see it.",
      hard_skill_page_grad_4: 'actively wrote projects in this language, longer experience in using it.',
      hard_skill_page_grad_5: 'know quite a bit about the technology, experience in real projects.',
      hard_skill_page_grad_6: 'the level where you can write a frontend in Java.',
      hard_skill_page_pl: 'Programming languages',
      hard_skill_page_fw: 'Frameworks',
      hard_skill_page_db: 'Databases',
      hard_skill_page_lang: 'Languages',
      hard_skill_page_other: 'Other',

      career_dates: 'Dates',
      career_responsibilities: 'Responsibilities',
      career_page_info:
        'Here is a description of my experience in companies and visiting really useful in my opinion events.',
      career_page_zl: 'ZetLab',
      career_page_zl_from: 'Feb 2019',
      career_page_zl_to: 'Sep 2019',
      career_page_zl_desc:
        'the company was selling a B2B product, its main task was to link 1C and the site from which the order is made, to form reports for each client directly from 1C, etc... Development of new projects based on existing templates. Reworking of new features: adding mail services, bonus system, processing of price system, output of additional reports and so on... That is, the work was done on the frontend and backend, and on the 1C, which was engaged by the developer when he got the project. Communication with the customers was not supported by the developers.',
      career_page_am: 'AMemoryPro',
      career_page_am_from: 'Sep 2019',
      career_page_am_to: 'Dec 2019',
      career_page_am_desc:
        'the company was engaged in the development of a lot of different bots for Telegram, Viber, WhatsApp. In fact, I had time to write a couple of bots, study the documentation, but I did not make any super complicated bots.',
      career_page_inc: 'Incrify',
      career_page_inc_from: 'Dec 2019',
      career_page_inc_to: 'Sep 2020',
      career_page_inc_desc:
        'on the whole, the company lived off its quest room in Austria and the website that we maintained. But I came here for two big tasks. The first, developing a website for a new quest room: laser fights. We took care of everything, from the bookings to the contact page. The second, a project for UAI, it had to be a full-fledged platform on which users could search for various sporting events (workouts, marathons, races, swims...). The organizers created the events, users could register, pay, etc. Unfortunately, this project was never launched so, I do not know the details.',
      career_page_dl: 'DistributedLab',
      career_page_dl_from: 'Sep 2020',
      career_page_dl_to: 'Present',
      career_page_dl_desc:
        'I joined the company as a PHP developer, I was involved in a sports betting project, the project was outsourced, and then I became a senior developer. After some time, I joined the frontend team and helped with the development of the DEFI platform. Then I started to work with smart contracts, and so it turned out that I had completely moved to this part. There are other tasks in parallel, a few projects at different stages, from communication with the client to the completion of the work.',
      career_page_bh1: 'Blockchain Hackathon',
      career_page_bh1_desc:
        'for the first time participated in a hackathon, did crypto-game, collected a couple of not big prizes, in the end finished this game.',
      career_page_bua2: 'BlockchainUA',
      career_page_bua2_desc: 'attended events, interesting topics, acquaintances, useful for overall progress.',
      career_page_bh2: 'Blockchain Hackathon',
      career_page_bh2_desc:
        'were engaged in supply chain development, as it turned out, the idea was new to us, but not so much to the judges. But it was enlightening.',
      career_page_bua1: 'BlockchainUA',
      career_page_bua1_desc: 'attended events, interesting topics, acquaintances, useful for overall progress.',
      career_page_dspm: 'Data SPM',
      career_page_dspm_from: 'Jul 2018',
      career_page_dspm_to: 'Feb 2019',
      career_page_dspm_desc:
        'I designed metal structures, mainly for the construction of hangars, warehouses, shopping centers, etc. From the rough drawings, we created a 3d model and working drawings, which were sent to the metalwork factory.',
      career_page_ober: 'Oberbeton',
      career_page_ober_from: 'Sep 2017',
      career_page_ober_to: 'Jul 2018',
      career_page_ober_desc:
        'this company had its own factory of reinforced concrete products. Actually, under the factory on request, made drawings of reinforced concrete products, one of the Aushan in Kiev - my work.',

      project_page_about: 'About',
      project_page_my_attachment: 'My attachment',
      project_page_info:
        'Here is a list of some of the projects in which I have managed to participate on the backend and frontend sides.',
      project_page_lasertron_title: 'Lasertron',
      project_page_lasertron_about: 'a project about a quest room with laser tag battles.',
      project_page_lasertron_attachments: 'mostly backend, some frontend.',
      project_page_canna_title: 'Canna',
      project_page_canna_about: 'crypto grass growing game.',
      project_page_canna_attachments:
        'development of smart contracts according to business logic. Network deployment, Opensea deployment.',
      project_page_cero_title: 'Cero',
      project_page_cero_about: 'crypto game about NFT battles.',
      project_page_cero_attachments: 'the game was made entirely by me, from the design to the implementation.',
      project_page_dexe_title: 'DEXE',
      project_page_dexe_about:
        'ecosystem of various tools and platforms coming together as the go-to one-stop-shop for DeFi trading.',
      project_page_dexe_attachments: 'development of smart contracts for the governance.',
      project_page_event_title: 'Meraan Event',
      project_page_event_about: 'sports events project. Organize an event, buy training, schedule, etc...',
      project_page_event_attachments:
        'project management, solving problems that could not be solved by the backend or frontend guys.',
      project_page_meraan_title: 'Meraan Sport',
      project_page_meraan_about: 'fitnes events project. Organize a trainings, buy, schedule, etc...',
      project_page_meraan_attachments: 'participated both in the development of the backend and frontend.',
      project_page_nftb_title: 'NFTB',
      project_page_nftb_about: 'platform for digital ownership.',
      project_page_nftb_attachments: 'development of lunchpad on smart contracts and various tokensails.',
      project_page_q_title: 'Q',
      project_page_q_about: 'DEFI platform with its own coin and other features.',
      project_page_q_attachments: 'I helped with the development of the frontend for smart contracts.',
      project_page_rs_title: 'RS4U',
      project_page_rs_about: 'the company sells a variety of spare parts.',
      project_page_rs_attachments: 'backend and frontend development, integration with 1C.',
      project_page_schedule_title: 'Schedule',
      project_page_schedule_about: 'platform for school schedules',
      project_page_schedule_attachments: 'the platform was made entirely by me, from the design to the implementation.',
      project_page_tip_title: 'TIP1.com',
      project_page_tip_about: 'a platform for betting on sporting events.',
      project_page_tip_attachments: 'support of existing backend and frontend, development of new features.',
    },
  },
  uk: {
    message: {
      global_home: 'Головна',
      global_about: 'Про мене',
      global_hard_skills: 'Хард скіли',
      global_career: "Кар'єра",
      global_projects: 'Проєкти',
      global_print: 'Друк',
      global_language: 'Мова',
      global_stack: 'Стек',

      home_page_info_1: 'Вітаю, на цій сторінці ви знайдете CV Олександра Федоренка.',
      home_page_info_2:
        'Цей ресурс поділено на секції для того, щоб Вам було легше сприймати інформацію. Якщо Вас цікавить сторінка де одразу вся інфа, клікайте',
      home_page_info_3: 'сюди',
      home_page_info_4: '',

      about_page_info: 'Тут трохи написано про мене та мої хобі.',
      about_page_photo: 'Місце для фото',
      about_page_intro: 'Вступ',
      about_page_intro_txt:
        'Привіт, я не дуже впевнений що це полотно будуть читати, але залишу це тут про всяк випадок. Почну, мене звуть Олександр Федоренко, я народився в 1995 році в Києві де і живу на цей час.',
      about_page_study: 'Навчання',
      about_page_study_txt:
        "Закінчивши 9 класів школи, пішов у коледж. У 2014 році я вступив до Національного Транспортного Університету на спеціальність 'Будівництво мостів та тунелів'. Тут Ви можете відмити що освіта в мене не айтішна, це така фіча.<br>Я здобув звичайний диплом бакалавра і червоний диплом магістра, на цьому я завершив з державною освітою.",
      about_page_job: 'Перша нормальна робота',
      about_page_job_txt:
        "Ще до того як я закінчив університет, мені запропонували роботу за спецальністю, я не водив носом і погодився, загалом встиг попрацювати за спеціальністю десь 2 роки, змінивши 2 роботи. Спеціальність сама по собі не погана і цікава, розробляєш креслення, на заводи їздиш, бачиш в реальному житті що ти там запроєктував, та інші плюшки.<br>Був тільки один серйозний недолік, малі зарплати, повільний кар'єрний ріст.",
      about_page_move: 'Хід конем',
      about_page_move_txt:
        'Я поспілкувався зі своїми друзями в айті і прийняв рішення перейти в нову сферу, але грошей на курси не було, тому вчився самостійно.<br>Звісно я не кинув усе і побіг вчити айті, бо треба було за щось жити, тому дізнавався щось нове в вільний від роботи час. Мій не великий досвід в .Net, який я отримав коли писав дипломну роботу, допоміг мені в навчанні. Загалом, за 3 місяці я написав простий сайт який робив деякі інженерні розрахунки, не самі прості, та і виглядав норм! Я зробив резюме та відправляв його на будь-які вакансії, взагалі не було різниці на чому там писати. Всі знають як важко попасти в айті, але ніфіга не важко, просто може вимоги та очікування завеликі.',
      about_page_career: "Нова кар'єра",
      about_page_career_txt:
        "Я зазвичай не відзиваюсь погано про своїх роботодавців, але на даний момент розумію що моя перша айті компанія була далеко від стандартів, але керівництво добре знало свою справу в програмуванні. На той час я міг лише здогадуватись що то за стандарти, але ситуація на той момент часу мене не лякала.<br>В цілому, було круто що вони приймали новачків та давали шанс вчитися. Там я здобув свій перший досвід та пішов підкорювати нові вершини.<br>Я влаштувався в нову компанію, але довго там не пропрацював, зрозумів що боти - не моє, було не дуже цікаво, тому пішов далі.<br>В наступній компанії я нарешті зрозумів переваги класного офісу, чекав цілий рік! Працювалось не погано, гарний колектив, офіс, ковід, віддалена робота, класика. Таким чином я пропрацював цілий рік, зобов'язання зросли, але заробітна платня залишалась тією самою, тому я пішов в іншу компанію.<br>Що ж, я досі працюю в цій новій компанії, вона дала мені багато досвіду в різних сферах та відкрила нові шляхи, в тому числі до блокчейну.",
      about_page_outside: 'Поза роботою',
      about_page_outside_txt:
        'Коли був вільний час, я зробив два досить великих проєкти. Перший, платформа для керування розкладом навчальних закладів, там є авторизація, адмінка та інше. Складність була в тому, що потрібно уніфікувати різні підходи до розкладів в різних навчальних закладах та скласти це все разом. Загалом, це був чудовий досвід. Другий, децентралізована гра, ще один цікавий проєкт. Також відвідував хакатони та конференції, на хакатонах навіть гроші вдавалось збирати!',
      about_page_hobby: 'Хобі',
      about_page_hobby_txt:
        'Мені подобаються різні активності, особливо екстремальні: стрільба, стрибки с парашутом, туризм, плавання... Із більш сидячих: читання, пазли, робити щось своїми руками.',

      hard_skill_page_info: 'На цій сторінці ви можете знайти моє приблизне бачення на технічні скіли.',
      hard_skill_page_grad: 'Шкала вимірювання',
      hard_skill_page_grad_1: 'зустрічався, але не було можливості попрактикуватись на реальних проєктах.',
      hard_skill_page_grad_2: 'трохи працював з цією мовою або фреймворком.',
      hard_skill_page_grad_3: 'використовував у роботі, не розгублюсь якщо доведеться зіштовхнутись.',
      hard_skill_page_grad_4: 'було декілька проєктів, більш поглиблені знання.',
      hard_skill_page_grad_5: 'знаю достатньо про технологію, можу вирішувати багато задач.',
      hard_skill_page_grad_6: 'рівень коли можна написати фронтенд на Java.',
      hard_skill_page_pl: 'Мови програмування',
      hard_skill_page_fw: 'Фреймворки',
      hard_skill_page_db: 'Бази данних',
      hard_skill_page_lang: 'Мови',
      hard_skill_page_other: 'Інше',

      career_dates: 'Дати',
      career_responsibilities: "Обов'язки",
      career_page_info: 'На цій сторінці інформація про мій досвід в різних компаніях та участь у цікавих заходах.',
      career_page_zl: 'ZetLab',
      career_page_zl_from: 'Лютий 2019',
      career_page_zl_to: 'Вересень 2019',
      career_page_zl_desc:
        "компанія займалася продажем B2B продукту, основна задача якого зв'язати 1C та вебсайт з якого приходять замовлення для того щоб користувач міг формувати звітність напряму з 1С. Розробка нових проєктів відбувалась на основі існуючого шаблону. Робились наступні допрацювання: додавання нового способу доставки, бонусні системи, перероблення цінової політики, додавання нових звітів та інше. Таким чином розробник вів роботи на фронтенді, бекенді та на 1С. Єдине чим я не займався - спілкування з клієнтами.",
      career_page_am: 'AMemoryPro',
      career_page_am_from: 'Вересень 2019',
      career_page_am_to: 'Грудень 2019',
      career_page_am_desc:
        'компанія займалася розробкою ботів для Telegram, Viber, WhatsApp. По факту, я встиг написати декілька ботів, ознайомитись з документацією, але не займався розробкою складних ботів.',
      career_page_inc: 'Incrify',
      career_page_inc_from: 'Грудень 2019',
      career_page_inc_to: 'Вересень 2020',
      career_page_inc_desc:
        'в цілому, компанія існує за рахунок квест-кімнати в Австрії, розробники займались підтримкою існуючого сайту. Я займався двома великими завданнями. Перше, розробка сторінок для нової квест кімнати, яка відрізнялася від інших, лазерні бої. Ми робили усе, від букінга до сторінки контактів. Друге, проєкт для ОАІ, це буда повноцінна платформа на якій користувач міг знаходити спортивні івенти (забіги, марафони, йога, бокс...), та реєструватися на них, проводити оплату, управляти букінгами та інше. На жаль, проєкт так і не запустився, деталі не відомі.',
      career_page_dl: 'DistributedLab',
      career_page_dl_from: 'Вересень 2020',
      career_page_dl_to: 'Теперішній час',
      career_page_dl_desc:
        'Я приєднався до компанії як PHP розробник, на проєкт по ставках на спортивні заходи, проєкт був на аутсорсі, і пізніше я став лідом цього проєкту. Через деякий час я приєднався до команди фронтенду та допомагав з DEFI платформою. Потім я почав працювати зі смарт контрактами і перейшов більше у цю сферу, але усе одно паралельно мав інші задачі по різним проєктам, від комунікації з замовниками до активного програмування.',
      career_page_bh1: 'Blockchain Hackathon',
      career_page_bh1_desc:
        'вперше приймав участь у хакатоні, робив гру на блокчейні, вдалося зібрати декілька не великих призів. Пізніше таки завершив свою гру.',
      career_page_bua2: 'BlockchainUA',
      career_page_bua2_desc: 'відвідував цей захід, цікаві теми, нові знайомства, корисно для загального розвитку.',
      career_page_bh2: 'Blockchain Hackathon',
      career_page_bh2_desc:
        'ми займались розробкою ланцюга постачання с підтвердженнями в блокчейні. Як виявилось, ідея була не нова, але про це більше знали судді ніж ми, але отримали цікавий досвід.',
      career_page_bua1: 'BlockchainUA',
      career_page_bua1_desc: 'відвідував цей захід, цікаві теми, нові знайомства, корисно для загального розвитку.',
      career_page_dspm: 'Data SPM',
      career_page_dspm_from: 'Липень 2018',
      career_page_dspm_to: 'Лютий 2019',
      career_page_dspm_desc:
        'займався проєктуванням металевих конструкцій, ангарів, складів, торгівельних центрів та іншого. В цілому, робили с чорнових креслень 3D модель, а з неї вже робочі креслення, які пізніше відправлялися на завод металевих конструкцій.',
      career_page_ober: 'Oberbeton',
      career_page_ober_from: 'Вересень 2017',
      career_page_ober_to: 'Липень 2018',
      career_page_ober_desc:
        'ця компанія має власний завод залізобетонних виробів. Загалом, займався розробкою креслень нових проєктів, які виготовлялись під замовлення. Один із Ашанів в Києві - моя робота.',

      project_page_about: 'Про проєкт',
      project_page_my_attachment: 'Мій вклад',
      project_page_info: 'На цій сторінці список деяких проєктів у яких я приймав участь у тій чи іншій мірі.',
      project_page_lasertron_title: 'Lasertron',
      project_page_lasertron_about: 'проєкт розроблявся під нову квест-кімнату з лазерними боями.',
      project_page_lasertron_attachments: 'більша частина роботи - бекенд, деякі частини проєкту на фронтенді.',
      project_page_canna_title: 'Canna',
      project_page_canna_about: 'крипто гра по вирощуванню канабісу.',
      project_page_canna_attachments:
        'розробка смарт контрактів у відповідності до бізнес вимог. Розгортування в блокчейні, розгортування в Opensea.',
      project_page_cero_title: 'Cero',
      project_page_cero_about: 'крипто гра про бої NFT.',
      project_page_cero_attachments: 'гра була придумана і розроблена мною, від ідеї і до реалізації.',
      project_page_dexe_title: 'DEXE',
      project_page_dexe_about:
        'екосистема різноманітних інструментів і платформ, які об’єднуються як універсальний магазин для торгівлі DeFi.',
      project_page_dexe_attachments: 'розробка смарт контрактів для системи управління.',
      project_page_event_title: 'Meraan Event',
      project_page_event_about: 'проєкт по спортивним заходам. Організація заходів, купівля білетів, розклад та інше.',
      project_page_event_attachments:
        'Менеджмент проєкта, вирішення питань на бекенді та фронтенді, які не змогла вирішити команда.',
      project_page_meraan_title: 'Meraan Sport',
      project_page_meraan_about:
        'проєкт по спортивним тренуванням. Організація заходів, купівля білетів, розклад та інше..',
      project_page_meraan_attachments: 'працював як на бекенді так і на фронтенді.',
      project_page_nftb_title: 'NFTB',
      project_page_nftb_about: 'платформа для цифрової власності.',
      project_page_nftb_attachments: 'розробка лаунчпаду на смарт контрактах, різних токенсейлів.',
      project_page_q_title: 'Q',
      project_page_q_about: 'DEFI платформа зі своїм блокчейном, системою керування та іншим.',
      project_page_q_attachments: 'добомогав із фронтендом для смарт контрактів.',
      project_page_rs_title: 'RS4U',
      project_page_rs_about: 'команія продає різні запчастини, сайт-магазин.',
      project_page_rs_attachments: 'бекенда та фронтенд, інтеграція з 1С.',
      project_page_schedule_title: 'Schedule',
      project_page_schedule_about: 'платформа на якій можна знайти розклади різних навчальних закладів.',
      project_page_schedule_attachments: 'Платформа була розроблена мною, власне, як і ідея.',
      project_page_tip_title: 'TIP1.com',
      project_page_tip_about: 'платформа для ставок на спортивні події.',
      project_page_tip_attachments: 'підтримка існуючого бекенду та фронтенду, розробка нових фіч.',
    },
  },
};

export default messages;
