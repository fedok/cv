import Vue from 'vue';
import VueRouter from 'vue-router';
import HomePage from 'src/components/pages/HomePage.vue';
import AboutPage from 'src/components/pages/AboutPage.vue';
import HardSkillPage from 'src/components/pages/HardSkillPage.vue';
import CareerPage from 'src/components/pages/CareerPage.vue';
import ProjectPage from 'src/components/pages/ProjectPage.vue';
import PrintPage from 'src/components/pages/PrintPage.vue';

Vue.use(VueRouter);

const routes = [
  { path: '/', component: HomePage, name: 'home-page' },
  { path: '/about', component: AboutPage, name: 'about-page' },
  { path: '/hard-skills', component: HardSkillPage, name: 'hard-skill-page' },
  { path: '/career', component: CareerPage, name: 'career-page' },
  { path: '/projects', component: ProjectPage, name: 'project-page' },
  { path: '/print', component: PrintPage, name: 'print-page' },
];

const router = new VueRouter({
  mode: 'history',
  // base: process.env.BASE_URL,
  routes,
});

export default router;
