module.exports = {
  root: true,
  extends: ['plugin:vue/essential', 'plugin:prettier/recommended', 'eslint:recommended'],
  env: {
    browser: true,
    node: true,
  },
  rules: {
    'vue/max-len': ['error', { code: 120, template: 120 }],
    'prettier/prettier': ['error', { singleQuote: true, printWidth: 120 }],
  },
};
