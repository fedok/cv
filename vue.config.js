const path = require('path');
const { defineConfig } = require('@vue/cli-service');

module.exports = defineConfig({
  transpileDependencies: true,
  configureWebpack: {
    resolve: {
      alias: {
        src: path.resolve(__dirname, 'src'),
      },
    },
  },
  chainWebpack: (config) => {
    config.resolve.alias.set('@', path.join(__dirname, './src'));
  },
});
