### Contacts

**Developer**: Oleksandr Fedorenko

**Email**: agoodminute@gmail.com

**Telegram**: @agoodminute

### Commands

`npm install` - project setup

`npm run serve` - compiles and hot-reloads for development

`npm run build` - compiles and minifies for production
